package core

import (
	"io/ioutil"
	"log"
	"os"
	"encoding/json"
	"strings"
)

type Configuration struct {
	SendTo string `json:"sendTo"`
	Frequency int `json:"frequency"`
	Processes string `json:"processes"`
	HostName string `json:"hostName"`
	Domain string `json:"domain"`
	LPA LPAData `json:"lpa"`
	OS string `json:"os"`
	// TODO authentication here...
}

func LoadConfiguration(file string) Configuration {
	jsonFile, err := os.Open(file)
	if err != nil {
		log.Fatal(jsonFile, "\nError: ", err)
	}
	defer jsonFile.Close()

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatal(byteValue, "\nError: ", err)
	}
	var c Configuration
	json.Unmarshal(byteValue, &c)
	return c
}

func (c Configuration) GetProcesses() []string {
	parts := strings.Split(c.Processes, ";")
	var results []string

	for _, part := range parts {
		tp := strings.TrimSpace(part)
		if len(tp) > 0 {
			results = append(results, tp)
		}
	}

	return results
}