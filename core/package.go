package core

// #cgo LDFLAGS: -L${SRCDIR}/../csalp/lib -lcsalp
// #include "../csalp/csalp.h"
import "C"

import (
	"encoding/json"
	"fmt"
)

type CPackage struct {
	Name string `json:"name"`
	Desc string `json:"desc"`
	Arch string `json:"arch"`
	Version string `json:"version"`
	State string `json:"state"`
	Maintainer string `json:"maintainer"`
	OriginalMaintainer string `json:"originalMaintainer"`
}

type CPackages struct {
	Packages []CPackage `json:"packages"`
}

func GetPackages(config Configuration) []SoftwareObject {
	jsonPackages := C.get_packages(C.CString(config.OS))

	var cps CPackages
	err := json.Unmarshal([]byte(C.GoString(jsonPackages)), &cps)
	if err != nil {
		fmt.Println("There was an error:", err)
	}

	var sos []SoftwareObject
	for _, cp := range cps.Packages {
		so := SoftwareObject{"", cp.Name, cp.Maintainer, cp.Version}
		sos = append(sos, so)
	}

	return sos
}