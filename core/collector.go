package core

import (
	"errors"
	"log"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/mem"
	"time"
	"github.com/shirou/gopsutil/host"
	"net"
)

type Collector interface {
	Name() string
	Desc() string
	Collect() HostData
	Stop() bool
	GetAnonymizer() Anonymizer
}

func GetHardwareData() HardwareData {
	cinfo, err := cpu.Info()
	if err != nil {
		log.Fatal(cinfo, "\nError: ", err)
	}

	minfo, err := mem.VirtualMemory()
	if err != nil {
		log.Fatal(minfo, "\nError: ", err)
	}

	return HardwareData{cinfo[0].VendorID, cinfo[0].Family, cinfo[0].ModelName,
		len(cinfo), minfo.Total}
}

func GetOSData() SoftwareObject {
	info, err := host.Info()
	if err != nil {
		log.Fatal(info, "\nError: ", err)
	}
	// TODO review here
	return SoftwareObject{info.OS, info.Platform, info.PlatformVersion, info.KernelVersion}
}

func ExternalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("are you connected to the network?")
}


func GetCurrentTimeStamp() string {
	t := time.Now()
	return t.Format(time.RFC3339)
}


