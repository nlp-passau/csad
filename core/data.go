package core

import "time"

type LPAData struct {
	Id string `json:"id"`
	Name string `json:"name"`
}

type HardwareData struct {
	ProcVendor string `json:"proc_vendor"`
	ProcFamily string `json:"proc_family"`
	ProcModel string `json:"proc_model"`
	NumProc int `json:"num_proc"`
	MemorySize uint64 `json:"memory_size"`
}

type SoftwareObject struct  {
	Description string `json:"description"`
	Name string `json:"name"`
	Vendor string `json:"vendor"`
	Version string `json:"version"`
}

type HostData struct {
	Created time.Time`json:"created"`
	IP string `json:"ipv4_addr"`
	Domain string `json:"domain"`
	LPA LPAData `json:"lpa"`
	Hardware HardwareData `json:"hardware"`
	OS SoftwareObject `json:"os"`
	Packages []SoftwareObject `json:"packages"`
}

type OSCollector struct {
	configuration Configuration
}