package core

import "time"

type DefaultCollector struct {
	Configuration Configuration
}

func (DefaultCollector) Name() string {
	return "DefaultCollector"
}

func (DefaultCollector) Desc() string {
	return "DefaultCollector"
}

func (c DefaultCollector) Collect() HostData {
	hw := GetHardwareData()
	os := GetOSData()
	ip, _ := ExternalIP()
	return HostData{time.Now(), ip, c.Configuration.Domain, c.Configuration.LPA,
	hw, os, GetPackages(c.Configuration)}
}

func (DefaultCollector) Stop() bool {
	return true
}

func (DefaultCollector) GetAnonymizer() Anonymizer {
	return nil
}
