package factory

import (
	"log"
	"gitlab.com/nlp-passau/csad/core"
	"gitlab.com/nlp-passau/csad/comm"
	"gitlab.com/nlp-passau/csad/mock"
)

func BuildSender(sdtio bool, configuration core.Configuration) *comm.Sender {
	var sender comm.Sender
	if sdtio {
		sender = comm.SdtIOSender{configuration}
	} else {
		sender = comm.HttpSender{configuration}
	}

	return &sender
}

func BuildCollectors(configuration core.Configuration) []core.Collector {
	processesNames := configuration.GetProcesses()
	processes := make([]core.Collector, len(processesNames))

	for i, name := range processesNames {
		var process core.Collector
		switch name {
		case "mock":
			process = mock.Collector{configuration}
		case "default":
			process = core.DefaultCollector{configuration}
		default:
			log.Fatal(process, "\nError: ", "invalid process name")
		}

		processes[i] = process
	}
	return processes
}
