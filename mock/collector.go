package mock

import (
	"gitlab.com/nlp-passau/csad/core"
	"time"
)

type Collector struct {
	Configuration core.Configuration
}

func (m Collector) Name() string {
	return "MockCollector"
}

func (m Collector) Desc() string {
	return "mock collector for test purposes"
}

func GetSoftwareObjects() []core.SoftwareObject {
	objects := make([]core.SoftwareObject, 3)
	software := core.SoftwareObject{"", "Oracle", "JDK", "1.8"}
	objects[0] = software
	objects[1] = software
	objects[2] = software

	return objects
}

func (m Collector) Collect() core.HostData {
	hw := core.GetHardwareData()
	os := core.GetOSData()
	ip, _ := core.ExternalIP()
	return core.HostData{time.Now(), ip, m.Configuration.Domain, m.Configuration.LPA,
	hw, os, GetSoftwareObjects()}
}

func (m Collector) Stop() bool {
	return true
}

func (m Collector) GetAnonymizer() core.Anonymizer {
	return nil
}