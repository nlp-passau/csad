package main

import (
	"time"
	"github.com/kardianos/service"
	"gitlab.com/nlp-passau/csad/comm"
	"gitlab.com/nlp-passau/csad/core"
	"gitlab.com/nlp-passau/csad/factory"
)

type Daemon struct {
	ticker *time.Ticker
	sender *comm.Sender
	collectors []core.Collector
}

func NewDaemon(config core.Configuration) *Daemon {
	home, config := getConfigurationFile()
	configureLogFiles(home)
	Trace.Printf("Configuration file:\n%+v", config)

	ticker := time.NewTicker(time.Duration(config.Frequency) * time.Millisecond)
	sender := factory.BuildSender(false, config)
	collectors := factory.BuildCollectors(config)

	Trace.Print("New Daemon!")
	return &Daemon{ticker, sender, collectors}
}

func (d *Daemon) Start(s service.Service) error {
	Trace.Print("Starting service...")
	go d.run()
	return nil
}

func (d *Daemon) run() {
	Trace.Print("Service running!")

	for _ = range d.ticker.C {
		Trace.Print("Running collectors...")
		for _, collector := range d.collectors {
			data := collector.Collect()
			(*d.sender).Send(&data)
		}
		Trace.Print("Collectors finished!\ngoing to sleep...")
	}
}

func (d *Daemon) Stop(s service.Service) error {
	d.ticker.Stop()
	return nil
}


