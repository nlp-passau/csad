package main

import (
	"log"
	"os"
	"path"

	"github.com/mitchellh/go-homedir"

	"gitlab.com/nlp-passau/csad/core"
	"github.com/kardianos/service"
	"flag"
)

const DEFAULT_DIR = "/opt/csad"
const DEFAULT_HOME_DIR = ".csad"

var (
	Trace   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
)



func main() {
	home, config := getConfigurationFile()
	configureLogFiles(home)
	Trace.Printf("Configuration file:\n%+v", config)

	svcConfig := &service.Config{
		Name:        "csad",
		DisplayName: "csad",
		Description: "the cs-aware daemon",
	}

	svcFlag := flag.String("service", "", "Control the system service.")
	flag.Parse()

	prg := NewDaemon(config)

	s, err := service.New(prg, svcConfig)
	if err != nil {
		Error.Fatal(err)
	}

	if len(*svcFlag) != 0 {
		err := service.Control(s, *svcFlag)
		if err != nil {
			log.Printf("Valid actions: %q\n", service.ControlAction)
			log.Fatal(err)
		}
		return
	}

	err = s.Run()
	if err != nil {
		Error.Fatal(err)
	}

	Trace.Print("Service finished!")
}

func getConfigurationFile() (string, core.Configuration) {
	home, err := homedir.Dir()
	if err != nil {
		log.Println("Failed to get the home dir.", err)
	}

	var configFile = ""
	if _, err := os.Stat(path.Join(DEFAULT_DIR, "config.json")); !os.IsNotExist(err) {
		configFile = path.Join(DEFAULT_DIR, "config.json")
	} else if _, err := os.Stat(path.Join(home, DEFAULT_HOME_DIR, "config.json")); !os.IsNotExist(err) {
		configFile = path.Join(home, DEFAULT_HOME_DIR, "config.json")
	}

	if configFile == "" {
		log.Fatalf("Cannot find a configuration file. Please, put 'config.json' in either '%s' or '%s'. "+
			"Check the file 'config_example.json' in the project repository to see its structure.",
			DEFAULT_DIR, path.Join(home, DEFAULT_HOME_DIR))
	}

	config := core.LoadConfiguration(configFile)
	return home, config
}

func configureLogFiles(home string) {
	logDir := path.Join(home, DEFAULT_HOME_DIR, "log")
	if _, err := os.Stat(logDir); os.IsNotExist(err) {
		err = os.MkdirAll(logDir, os.ModePerm)
		if err != nil {
			log.Fatalf("Cannot CREATE the log dir at '%s'.", logDir)
		}
	}

	f, err := os.OpenFile(path.Join(logDir, "trace.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		log.Fatalf("Cannot CREATE log file: trace.log")
	}

	Trace = log.New(f,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	f, err = os.OpenFile(path.Join(logDir, "info.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		log.Fatalf("Cannot CREATE log file: info.log")
	}
	Info = log.New(f,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	f, err = os.OpenFile(path.Join(logDir, "warning.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		log.Fatalf("Cannot CREATE log file: warning.log")
	}
	Warning = log.New(f,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	f, err = os.OpenFile(path.Join(logDir, "error.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		log.Fatalf("Cannot CREATE log file: error.log")
	}
	Error = log.New(f,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}
