package comm

import (
	"gitlab.com/nlp-passau/csad/core"
	"encoding/json"
	"fmt"
	"net/http"
	"bytes"
	"log"
)

type Sender interface {
	Configuration() core.Configuration
	Send(data *core.HostData) bool
}

type SdtIOSender struct {
	Config core.Configuration
}

func (s SdtIOSender) Configuration() core.Configuration {
	return s.Config
}

func (s SdtIOSender) Send(data *core.HostData) bool {
	j, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		fmt.Println(err)
		return false
	}

	fmt.Println(string(j))
	return true
}

type HttpSender struct {
	Config core.Configuration
}

func (s HttpSender) Configuration() core.Configuration {
	return s.Config
}

func (s HttpSender) Send(data *core.HostData) bool {
	j, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest("POST", s.Config.SendTo, bytes.NewBuffer(j))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return false
	}

	defer resp.Body.Close()
	return resp.StatusCode == 200
}

